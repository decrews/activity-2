﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Launcher : MonoBehaviour {

	public float launchBoost = 60;
	public float maxAngle = 80f;
	public float minAngle = 3f;

	public GameObject pigPrefab;

	Vector3 direction;

	bool readyToShoot = true;

	// Use this for initialization
	void Start () {
		
	}

	void Update() {
		// Ensure the player can only shoot the pig when it is in the cannon.
		if (Input.GetMouseButtonDown(0)) {
			if (readyToShoot && GameManager.ammo > 0) {
				launchPig (direction);
			}
		}

		// Allow you to exit the game with escape
		if (Input.GetKey (KeyCode.Escape)) {
			Debug.Log ("You pressed escape");
			Application.Quit ();
		}

		// Checks to see if there is a pig currently active in the game
		// to limit shooting.
		if (GameObject.Find("Piggy(Clone)")) {
			readyToShoot = false;
		} else {
			readyToShoot = true;
		}
	}

	// Update is called once per frame
	void FixedUpdate () {
		Vector3 mouseInWorld = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 10f));
		//Debug.Log (mouseInWorld);
		direction = mouseInWorld - transform.position;

		Quaternion rotation = Quaternion.LookRotation (direction);
		Quaternion checkRot = new Quaternion (0, 0, rotation.z, rotation.w);

		if (checkRot.eulerAngles.z >= minAngle && checkRot.eulerAngles.z <= maxAngle) {
			transform.rotation = checkRot;
		}
	}

	void launchPig(Vector3 direction) {
		GameObject pig = Instantiate (pigPrefab, transform.position + new Vector3(1,1,0), Quaternion.identity);

		// Unparent the pig from the cannon
		pig.transform.parent = null;

		// Enabled the sprite
		pig.GetComponent<SpriteRenderer>().enabled = true;

		// Launch the pig via its rigidbody
		Rigidbody2D rbPig = pig.gameObject.GetComponent<Rigidbody2D>();
		rbPig.gravityScale = 1;
		rbPig.AddForce(new Vector2(direction.x * launchBoost, direction.y * launchBoost));
		GameManager.ammo -= 1;

		// Print the remaining ammo
		Debug.Log(GameManager.ammo + " shots remaining");
	}
}
