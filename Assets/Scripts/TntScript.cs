﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TntScript : MonoBehaviour {

	public GameObject explosionPrefab;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnCollisionEnter2D(Collision2D collider) {
		if(collider.gameObject.CompareTag("Construction")
			|| collider.gameObject.CompareTag("Ground")) {
			Instantiate (explosionPrefab, transform.position, Quaternion.identity);
			Destroy (this.gameObject);
		}

		if(collider.gameObject.CompareTag("Bird")) {
			GameManager.ammo += 1;
			Destroy(collider.gameObject);
		}
	}
}
