﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

	public static int score = 0;
	public static int ammo = 5;
	static GameObject[] birdsRemaining;
	bool playerActive;

	Text scoreString;
	Text livesString;
	Text endString;

	// Use this for initialization
	void Start () {
		scoreString = GameObject.Find ("Score Text").GetComponent<Text> ();
		livesString = GameObject.Find ("Lives Remaining").GetComponent<Text> ();
		endString = GameObject.Find ("End Text").GetComponent<Text> ();
	}
	
	// Update is called once per frame
	void Update () {
		// Check to see if a pig is still active
		if (GameObject.Find ("Piggy(Clone)")) {
			playerActive = true;
		} else {
			playerActive = false;
		}

		// u
		birdsRemaining = GameObject.FindGameObjectsWithTag("Bird");
		scoreString.text = "Score: " + score;
		livesString.text = "Pigs Remaining: " + ammo;


		if (ammo == 0 && birdsRemaining.Length != 0
			&& playerActive == false) {
			endString.text = "You lose";
			endString.color = Color.red;
			Time.timeScale = 0;
		} else if (birdsRemaining.Length == 0) {
			endString.text = "You win!";
			endString.color = Color.yellow;
			Time.timeScale = 0;
		}
	}
}
