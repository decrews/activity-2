﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockScript: MonoBehaviour {
	bool isHit = false;
	public GameObject scoreDisplay;
	Rigidbody2D rb;


	// Use this for initialization
	void Start () {
		rb = gameObject.GetComponent<Rigidbody2D> ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void FixedUpdate() {
		if (rb.mass < 25 && rb.velocity.magnitude > 10) {
			Destroy (this.gameObject);
		}
	}

	void OnCollisionEnter2D(Collision2D collision) {
		if (collision.gameObject.CompareTag ("Player") && isHit == false) {
			Debug.Log ("Testing collision with player");
			addScore (10);
			isHit = true;
		} else if (collision.gameObject.CompareTag ("Ground")) {
			addScore (15);
		} else if (collision.gameObject.CompareTag ("Explosion")) {
			Destroy (this.gameObject);
		}
	}

	public void addScore(int amt) {
		GameManager.score += amt;
		Instantiate (scoreDisplay, new Vector3(transform.position.x, transform.position.y, -2), Quaternion.identity);
		Debug.Log ("Score: " + GameManager.score);

	}
}
