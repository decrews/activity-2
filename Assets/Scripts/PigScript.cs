﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PigScript : MonoBehaviour {

	public Sprite normalSprite;
	public Sprite bombSprite;
	public Sprite wingSprite;
	public Sprite bombWingSprite;

	public GameObject bombExplosion;

	bool wingPowerup = false;
	bool bombPowerup = false;

	bool exploded = false;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.Space)) {
			if (wingPowerup == true) {
				Debug.Log ("You got a boost!");
				gameObject.GetComponent<Rigidbody2D> ().AddForce (new Vector2 (400f, 3000f));

				wingPowerup = false;
			}
		}

		// Ensures correct sprite
		if (wingPowerup == false && bombPowerup == false) {
			gameObject.GetComponent<SpriteRenderer> ().sprite = normalSprite;
		} else if (wingPowerup == true && bombPowerup == false) {
			gameObject.GetComponent<SpriteRenderer> ().sprite = wingSprite;
		} else if (wingPowerup == false && bombPowerup == true) {
			gameObject.GetComponent<SpriteRenderer> ().sprite = bombSprite;
		} else if (wingPowerup == true && bombPowerup == true) {
			gameObject.GetComponent<SpriteRenderer> ().sprite = bombWingSprite;
		}
	}

	void OnTriggerEnter2D(Collider2D collider) {
		if (collider.gameObject.name == "Bomb Powerup") {
			//Destroy (collider.gameObject);
			bombPowerup = true;
		} else if (collider.gameObject.name == "Wings Powerup") {
			//Destroy (collider.gameObject);
			wingPowerup = true;
		}
	}

	void OnCollisionEnter2D(Collision2D collider) {
		if (collider.gameObject.CompareTag ("Construction")) {
			//collider.gameObject.GetComponent<BlockScript> ().addScore (10);
		}

		if(collider.gameObject.CompareTag("Construction")
			|| collider.gameObject.CompareTag("Ground")) {
			if (bombPowerup && exploded == false) {
				StartCoroutine (explosionTimer (0.5f));
				exploded = true;
			} else {
				StartCoroutine (resetPlayer ());
			}
		}

		if(collider.gameObject.CompareTag("Bird")) {
			GameManager.ammo += 2;
			Destroy(collider.gameObject);
		}
	}

	// Resets the player back to the cannon
	IEnumerator resetPlayer() {
		yield return new WaitForSeconds (3f);
		Destroy (this.gameObject);
	}

	// Coroutine to instantiate the explosion
	IEnumerator explosionTimer(float seconds) {
		yield return new WaitForSeconds (seconds);
		Instantiate (bombExplosion, transform.position, Quaternion.identity);
		gameObject.GetComponent<Rigidbody2D> ().isKinematic = true;
		gameObject.GetComponent<Rigidbody2D> ().velocity = new Vector3(0f,0f,0f);

		gameObject.GetComponent<SpriteRenderer> ().enabled = false;
		StartCoroutine (resetPlayer ());
	}
}
