﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
		StartCoroutine (destroyObject());
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	IEnumerator destroyObject() {
		yield return new WaitForSeconds (0.1f);
		gameObject.GetComponent<PointEffector2D> ().enabled = false;
	}
}
