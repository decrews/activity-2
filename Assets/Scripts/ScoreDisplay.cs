﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreDisplay : MonoBehaviour {

	// Use this for initialization
	void Start () {
		StartCoroutine (Display ());
	}
	
	// Update is called once per frame
	void Update () {
		transform.Translate(new Vector3(0f, 0.01f, 0));
	}

	IEnumerator Display() {
		yield return new WaitForSeconds (2f);
		Destroy (this.gameObject);
	}
}
