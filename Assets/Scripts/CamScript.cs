﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamScript : MonoBehaviour {
	float cameraOffset = 0f;
	GameObject pig;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		if (GameObject.Find ("Piggy(Clone)")) {
			pig = GameObject.Find ("Piggy(Clone)");

			if (pig.transform.position.x + cameraOffset > transform.position.x) {
				transform.position = new Vector3 (pig.transform.position.x + cameraOffset, 0, -10);
			}

			// Experimental Camera
			/*
			transform.position = Vector3.Lerp (transform.position,
				new Vector3(pig.transform.position.x, pig.transform.position.y, -10), 
				0.008f);
			*/
			/*
			else if (pig.GetComponent<Rigidbody2D> ().velocity.x < 0
				&& pig.transform.position.x - 1f < transform.position.x) {
				transform.position = Vector3.Lerp (transform.position,
					new Vector3(0, 0, -10), 
					0.01f);
			}
			*/
		} else {
			transform.position = Vector3.Lerp (transform.position,
				new Vector3(0, 0, -10), 
				0.02f);
		}
			/*
			if (pig.transform.position.x + cameraOffset > transform.position.x) {
				transform.position = new Vector3 (pig.transform.position.x + cameraOffset, 0, -10);
			*/
	}
}
